/// <reference types="cypress" />

describe('Search category', () => {

    it('can be switched when drop down menu is opened and is visible on the results list', () => {
        const newOption = 'TV Episodes'
        const secondOption = 'Keywords'

        cy.visit('https://www.imdb.com/')

        cy.get('#iconContext-arrow-drop-down')
          .click({animationDistanceThreshold: 20})
        
        cy.get('li')
          .contains(newOption)
          .click({force: true})

        cy.get('[for=navbar-search-category-select]')
          .should('have.text', newOption)

        cy.get('#iconContext-arrow-drop-down')
          .click({animationDistanceThreshold: 20})
     
        cy.get('li')
          .filter('[role=menuitem]')
          .last()
          .click()

        cy.get('[for=navbar-search-category-select]')
          .should('have.text', secondOption)

        cy.get('#suggestion-search')
          .type('test')
          .type('{enter}')

        cy.get('#findSubHeader')
          .contains(secondOption)
      })
})