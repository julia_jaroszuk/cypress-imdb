/// <reference types="cypress" />

const searchPhrase = 'abc'
const resultsForPhrase = `Results for \"${searchPhrase}\"`

describe('Perform search action by', () => {
    beforeEach(() => {
      cy.visit('https://www.imdb.com/')

      cy.get('#suggestion-search')
          .type(searchPhrase)
    })

    it('clicking search button', () => {
        cy.get('#suggestion-search-button')
          .click()

        cy.get('#main > div > h1')
          .should('have.text', resultsForPhrase)
    })

    it('pressing enter', () => {
        cy.get('#suggestion-search')
          .type('{enter}')

        cy.get('#main > div > h1')
          .should('have.text', resultsForPhrase)
    })
})