/// <reference types="cypress" />

describe('Initial test to check IMDB search functionality', () => {
    before(() => {
      cy.visit('https://www.imdb.com/')
    })

    it('search container exists and is not disabled', () => {
        cy.get('#suggestion-search-container')
          .should('exist')
          .and('not.be.disabled')
      })

    it('search box has placeholder text but no value', () => {
        cy.get('#suggestion-search')
          .invoke('attr', 'placeholder')
          .should('equal', 'Search IMDb')
    })

    it('default search option is "All"', () => {
        cy.get('[for=navbar-search-category-select]')
          .should('have.text', 'All')
    })

    it('there are 6 search options', () => {
        cy.get('li')
          .filter('[role=menuitem]')
          .should('have.length', 6)
    })

    it('search button exists and is enabled', () => {
      cy.get('#suggestion-search-button')
        .should('be.visible')
        .and('be.enabled')
    })
})
