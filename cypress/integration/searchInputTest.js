/// <reference types="cypress" />

describe('Search box input', () => {
    beforeEach(() => {
      cy.visit('https://www.imdb.com/')
    })

    it('value is the same as the search phrase that was typed in', () => {
        cy.get('#suggestion-search')
          .type('new search text')
          .should('have.value', 'new search text')
    })

    it('has no value after text field is cleared', () => {
        cy.get('#suggestion-search')
          .type('new search text')
          .should('have.value', 'new search text')
          .clear()
          .should('have.value','')
    })

    it('can be edited by the user after search phrase is typed in', () => {
        cy.get('#suggestion-search')
          .type('phrase')
          .should('have.value', 'phrase')
          .type('{backspace}')
          .should('have.value','phras')
          .type('al')
          .should('have.value', 'phrasal')
    })
})
