# Cypress IMDB



## Running tests locally

To run the tests locally, node and cypress need to be installed.

Tests can be run via command line:

    In the root directory, open command line and use one of the following commands:

        npx cypress run 

    or

        ./node_modules/.bin/cypress run

The tests can also be run via the Cypress Test Runner. It can be opened by using following command in the root directory:

    npx cypress open

    or

    npm run cypress:open


## Running tests in GitLab

The tests can also be run in the GitLab CI/CD.

Basic pipeline that runs the tests was added.

The tests will be run on Chrome browser.

Results of each run are accessible however no artifacts (screenshots/videos) are retained.